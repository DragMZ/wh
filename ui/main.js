BASE = "/api";

var v = new Vue({
    el: "#app",
    data: {
        auth: {
            form: {},
            current: {}
        },
        account: {}
    },
    methods: {
        get: function(uri) {
            return axios.get(BASE + uri, {auth: this.auth.form});
        },
        delete: function(uri) {
            return axios.delete(BASE + uri, {auth: this.auth.form});
        },
        login: function() {
            this.get("/account/" + encodeURIComponent(this.auth.form.username)).then(r => {
                this.auth.current = this.auth.form;
                this.account = r.data;
            });
        },
        refresh: function(e) {
            this.get("/account/" + encodeURIComponent(this.auth.current.username)).then(r => {
                this.account = r.data;
            });
        },
        disconnect: function(item) {
            this.delete("/account/"+ encodeURIComponent(this.auth.current.username) + "/listener/" + encodeURIComponent(item.id));
            this.account.listeners.splice(this.account.listeners.indexOf(item), 1);
        }
    },
    computed: {
        logged: function() {
            return this.account && this.account.name;
        }
    }
});