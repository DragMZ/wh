package log

import stdlog "log"

var (
	Level = 2
)

func Log(level int, args ...interface{}) {
	if Level < level {
		return
	}

	stdlog.Println(args...)
}

func Error(args ...interface{}) {
	Log(1, args...)
}

func Warn(args ...interface{}) {
	Log(2, args...)
}

func Debug(args ...interface{}) {
	Log(3, args...)
}

func Trace(args ...interface{}) {
	Log(4, args...)
}
