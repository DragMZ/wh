package wh

import (
	"net/http"
	"time"
)

type Request struct {
	Hook      string            `json:"hook"`
	At        time.Time         `json:"at"`
	Listeners []string          `json:"listeners"`
	IP        string            `json:"ip"`
	Method    string            `json:"method"`
	Error     string            `json:"error"`
	URI       string            `json:"uri"`
	Protocol  string            `json:"protocol"`
	Headers   map[string]string `json:"headers"`
}

type TriggerRequestResult struct {
	Error     error
	Listeners []string
}

type RequestsDB interface {
	Register(h *http.Request, t *TriggerRequest, r TriggerRequestResult) error
	Get(account string) ([]Request, error)
}

type Account struct {
	ID string
}

type AccountsDB interface {
	Create(name, password string) error
	Get(name string) (*Account, error)
}

type BasicAuthDB interface {
	Auth(name, password string) error
}

type TokensDB interface {
	Generate(account string) (string, error)
	Verify(account, token string) error
}
