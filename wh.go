package wh

import (
	"context"
	"io"
	"net/url"
)

// ReaderFactory is used to create multiple readers for the same data
type ReaderFactory interface {
	Create() (io.Reader, error)
}

// EventReceiver is used to deliver events to the receivers
type EventReceiver interface {
	Send(ctx context.Context, e *Event) error
}

type EventReceiverFactory interface {
	Create() (EventReceiver, error)
}

// TriggersRegistry is used to trigger an event
type TriggersRegistry interface {
	Trigger(ctx context.Context, t *Trigger) (map[string][]string, error)
}

// AccountTriggers is used to get triggers registry for a specified account
type AccountTriggers interface {
	Get(account string) (TriggersRegistry, error)
}

// AccountHooks is used to get hooks registry for a specified account
type AccountHooks interface {
	Get(account string) (HooksRegistry, error)
}

// HooksRegistration describes an event registration for given hooks
type HooksRegistration interface {
	Unregister() error
}

type Listener struct {
	ID    string   `json:"id"`
	Hooks []string `json:"hooks"`
}

// HooksRegistry can be used to register for incoming events
type HooksRegistry interface {
	Register(hs []Hook, rcv EventReceiverFactory, cancel func()) (HooksRegistration, error)
	Cancel(id string) error
	GetListeners() ([]Listener, error)
}

// Event describes a webhook event
type Event struct {
	ID          string
	ContentType string
	Args        url.Values
	Data        io.Reader
}
