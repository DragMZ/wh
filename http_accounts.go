package wh

import (
	"encoding/json"
	"net/http"
	"wh/log"

	"github.com/pkg/errors"
)

type HTTPAccountHandler struct {
	Server    *AccountServer
	BasicAuth BasicAuthDB
}

func (s *HTTPAccountHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	err := s.handle(w, req)
	if err != nil {
		log.Error("HTTP request failed:", err)
		httpError(w, "an error has occurred", http.StatusInternalServerError)
	}
}

func (s *HTTPAccountHandler) handle(w http.ResponseWriter, req *http.Request) error {
	switch req.Method {
	case http.MethodDelete:
		return s.handleDELETE(w, req)
	case http.MethodPost:
		return s.handlePOST(w, req)
	case http.MethodGet:
		return s.handleGET(w, req)
	}

	httpError(w, "bad request", http.StatusBadRequest)
	return nil
}

func (s *HTTPAccountHandler) handleDELETE(w http.ResponseWriter, req *http.Request) error {
	parts := readPathParts(req.URL.Path)
	if len(parts) != 3 {
		http.Error(w, "not found", http.StatusNotFound)
		return nil
	}

	r, err := s.Server.Hooks.Get(parts[0])
	if err != nil {
		return errors.Wrap(err, "failed to find account")
	}

	if parts[1] != "listener" {
		http.Error(w, "not found", http.StatusNotFound)
		return nil
	}

	id := parts[2]
	log.Trace("cancelling listener:", id)

	return r.Cancel(id)
}

func (s *HTTPAccountHandler) handleGET(w http.ResponseWriter, req *http.Request) error {
	var user string

	parts := readPathParts(req.URL.Path)
	if len(parts) == 0 {
		if s.BasicAuth != nil {
			user, pass, ok := req.BasicAuth()
			if !ok {
				w.Header().Set("WWW-Authenticate", "Basic realm=\"Auth Realm\"")
				httpError(w, "auth required", http.StatusUnauthorized)
				return nil
			}

			err := s.BasicAuth.Auth(user, pass)
			if err != nil {
				httpError(w, "unauthorized", http.StatusUnauthorized)
				return nil
			}
		}
	} else {
		user = parts[0]
	}

	if user == "" {
		httpError(w, "missing username", http.StatusBadRequest)
		return nil
	}

	details, err := s.Server.GetAccountDetails(user)
	if err != nil {
		return errors.Wrap(err, "failed to get account listeners")
	}

	b, err := json.Marshal(details)
	if err != nil {
		return errors.Wrap(err, "failed to marshal data")
	}

	w.Header().Set("Content-Type", "application/json")

	_, err = w.Write(b)
	if err != nil {
		return errors.Wrap(err, "failed to write response data")
	}

	return nil
}

func (s *HTTPAccountHandler) handlePOST(w http.ResponseWriter, req *http.Request) error {
	switch req.URL.Path {
	case "":

	default:
		httpError(w, "not found", http.StatusNotFound)
		return nil
	}

	name := req.FormValue("name")
	if name == "" {
		httpError(w, "no name given", http.StatusBadRequest)
		return nil
	}

	password := req.FormValue("password")
	if password == "" {
		httpError(w, "no password given", http.StatusBadRequest)
		return nil
	}

	err := s.Server.Create(name, password)
	if err != nil {
		httpError(w, "bad request", http.StatusBadRequest)
		return nil
	}

	return nil
}
