package wh

import (
	"sync"

	"github.com/pkg/errors"
)

type memoryTokens struct {
	l *sync.RWMutex
	m map[string]map[string]struct{}
}

func NewMemoryTokens() (TokensDB, error) {
	return &memoryTokens{
		l: &sync.RWMutex{},
		m: make(map[string]map[string]struct{}),
	}, nil
}

func (t *memoryTokens) Generate(account string) (string, error) {
	t.l.Lock()
	defer t.l.Unlock()

	ts, ok := t.m[account]
	if !ok {
		ts = make(map[string]struct{})
		t.m[account] = ts
	}

	var token string
	var err error

	for {
		token, err = generateRandomString(32)
		if err != nil {
			return "", errors.Wrap(err, "failed to generate token")
		}

		_, ok := ts[token]
		if !ok {
			break
		}
	}

	ts[token] = struct{}{}

	return token, nil
}

func (t *memoryTokens) Verify(account, token string) error {
	t.l.RLock()
	defer t.l.RUnlock()

	ts, ok := t.m[account]
	if !ok {
		return errors.New("account has no tokens")
	}

	_, ok = ts[token]
	if !ok {
		return errors.New("token not found")
	}

	return nil
}
