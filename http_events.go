package wh

import (
	"context"
	"encoding/json"
	"io"
	"mime"
	"net/http"
	"strings"
	"sync"
	"wh/log"

	"github.com/pkg/errors"
)

// HTTPEventsHandler is a HTTP handler for receiving events
type HTTPEventsHandler struct {
	Server *EventsServer
}

type channelEventReceiver struct {
	ch chan<- *Event
}

func (r channelEventReceiver) Send(ctx context.Context, e *Event) error {
	select {
	case r.ch <- e:
		return nil
	case <-ctx.Done():
		return errors.Wrapf(ctx.Err(), "trigger cancelled")
	}
}

func (s *HTTPEventsHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	err := s.handle(w, req)
	if err != nil {
		log.Error("HTTP events request failed:", err)
		http.Error(w, "request failed", http.StatusInternalServerError)
	}
}

func (s *HTTPEventsHandler) receive(rctx context.Context, lr *ListenRequest) (*Event, error) {
	out := make(chan *Event)

	lctx, cancel := context.WithCancel(context.Background())

	// single event receiver factory is used here because a single HTTP request is capable of receiving a single response (event) only
	close, err := s.Server.Listen(lr, &singleEventReceiverFactory{
		l: &sync.Mutex{},
		r: &channelEventReceiver{
			ch: out,
		},
	}, cancel)

	if err != nil {
		return nil, errors.Wrap(err, "failed to listen")
	}
	defer close()

	log.Trace("listener waiting")

	select {
	case e := <-out:
		log.Trace("listener got event")
		return e, nil
	case <-lctx.Done():
		log.Trace("lctx done")
		return nil, errors.Wrap(lctx.Err(), "listen context error")
	case <-rctx.Done():
		log.Trace("rctx done")
		return nil, errors.Wrap(rctx.Err(), "request context error")
	}
}

func (s *HTTPEventsHandler) handle(w http.ResponseWriter, req *http.Request) error {
	parts := readPathParts(req.URL.Path)

	var err error
	var hooks []Hook

	switch req.Method {
	case http.MethodGet:
		if len(parts) != 2 {
			return errors.Errorf("unsupported events scheme: %v", req.URL.Path)
		}

		ids := strings.Split(parts[1], ",")
		for _, id := range ids {
			hooks = append(hooks, Hook{
				ID:   id,
				Body: true,
			})
		}
	case http.MethodPost:
		mt, _, err := mime.ParseMediaType(req.Header.Get("Content-Type"))
		if err != nil {
			return errors.Wrap(err, "failed to read Content-Type")
		}

		switch mt {
		case "application/json":
			var hd []HookConfig

			d := json.NewDecoder(req.Body)
			err = d.Decode(&hd)
			if err != nil {
				return errors.Wrap(err, "failed to read body JSON")
			}

			for _, h := range hd {
				hooks = append(hooks, Hook{
					ID:          h.ID,
					Body:        h.Body,
					Args:        h.Args,
					IPFilter:    MakeIPFilter(h.IPFilter),
					QueryFilter: h.Filters,
				})
			}
		default:
			return errors.New("unsupported content type")
		}
	}

	lr := &ListenRequest{
		Account: parts[0],
		Hooks:   hooks,
	}

	e, err := s.receive(req.Context(), lr)
	if err != nil {
		return errors.Wrap(err, "failed to receive event")
	}

	hdr := w.Header()
	if e.ContentType != "" {
		hdr.Set("Content-Type", e.ContentType)
	}
	hdr.Set("X-Hook-ID", e.ID)

	if e.Args != nil {
		hdr.Set("X-Hook-Args", e.Args.Encode())
	}

	if e.Data != nil {
		_, err = io.Copy(w, e.Data)
		if err != nil {
			return errors.Wrap(err, "failed to transfer event data")
		}
	}

	return nil
}
