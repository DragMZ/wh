package wh

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"wh/log"

	"github.com/pkg/errors"
)

// HTTPTriggerHandler is a http.Handler for triggering events
type HTTPTriggerHandler struct {
	Requests RequestsDB
	Server   *TriggerServer
}

func (s *HTTPTriggerHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	err := s.handle(w, req)
	if err != nil {
		log.Error("HTTP request failed:", err)
		httpError(w, "an error has occurred", http.StatusInternalServerError)
	}
}

type bytesReaderFactory struct {
	b []byte
}

type tracedReader struct {
	r io.Reader
}

func (r *tracedReader) Read(p []byte) (n int, err error) {
	n, err = r.r.Read(p)
	log.Trace("read n:", n, ", err:", err, "data:", p[:n])
	return
}

func (f *bytesReaderFactory) Create() (io.Reader, error) {
	return &tracedReader{r: bytes.NewReader(f.b)}, nil
}

func (s *HTTPTriggerHandler) handle(w http.ResponseWriter, req *http.Request) error {
	parts := readPathParts(req.URL.Path)

	if len(parts) != 2 {
		httpError(w, "invalid arguments", http.StatusBadRequest)
		return nil
	}

	// Need to read data here because otherwise req.Context() does not get signalled on client disconnect
	// TODO: handle large data?
	data, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return errors.Wrap(err, "failed to read request body")
	}

	ip := func(addr string) string {
		ss := strings.SplitN(addr, ":", 2)
		if len(ss) < 2 {
			return addr
		}

		return ss[0]
	}(req.RemoteAddr)

	r := &TriggerRequest{
		Account: parts[0],
		Trigger: Trigger{
			ID:   parts[1],
			IP:   ip,
			Args: req.URL.Query(),
			Type: req.Header.Get("Content-Type"),
			Data: &bytesReaderFactory{b: data},
			Wait: true,
		},
	}

	log.Debug("trigger request:", r)

	lids, err := s.Server.Trigger(req.Context(), r)

	if s.Requests != nil {
		rerr := s.Requests.Register(req, r, TriggerRequestResult{
			Error:     err,
			Listeners: lids[r.ID],
		})

		if rerr != nil {
			log.Error("Register request error:", rerr)
		}
	}

	if err != nil {
		httpError(w, "failed to trigger", http.StatusBadGateway)
		return nil
	}

	log.Trace("Triggered listeners:", lids)

	return nil
}
