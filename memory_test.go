package wh

import "testing"

func TestMemoryTokens(t *testing.T) {
	mt, err := NewMemoryTokens()
	if err != nil {
		t.Fatal(err)
	}

	token, err := mt.Generate("test")
	if err != nil {
		t.Fatal(err)
	}

	err = mt.Verify("test", token)
	if err != nil {
		t.Error(err)
	}
}
