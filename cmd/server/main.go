package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"
	"wh"
	"wh/log"

	"github.com/pkg/errors"
)

func interruptSignal() <-chan os.Signal {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	return c
}

func mustLoadTLSConfig(cert string, key string) *tls.Config {
	tc, err := loadTLSConfig(cert, key)
	if err != nil {
		panic(err)
	}

	return tc
}

func loadTLSConfig(cert string, key string) (*tls.Config, error) {
	if cert == "" {
		return nil, errors.New("cert may not be empty")
	}

	if key == "" {
		return nil, errors.New("key may not be empty")
	}

	cc, err := tls.LoadX509KeyPair(cert, key)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to load cert: %s, key: %s", cert, key)
	}

	tc := tls.Config{
		Certificates: []tls.Certificate{cc},
	}

	return &tc, nil
}

func main() {
	lnet := flag.String("net", "tcp", "network to listen on")
	laddr := flag.String("addr", ":0", "address to listen on")
	loglvl := flag.Int("log", log.Level, "log level")
	timeout := flag.Int("shutdown-timeout", 1, "shutdown timeout")
	cert := flag.String("cert", "", "ceritificate file")
	key := flag.String("key", "", "private key file path")
	db := flag.String("db", "", "database connection string")

	flag.Parse()

	log.Level = *loglvl

	var tlscfg *tls.Config

	if *cert != "" || *key != "" {
		tlscfg = mustLoadTLSConfig(*cert, *key)
		tlscfg.NextProtos = []string{"h2"}
	}

	var l net.Listener
	var err error

	if tlscfg != nil {
		l, err = tls.Listen(*lnet, *laddr, tlscfg)
	} else {
		l, err = net.Listen(*lnet, *laddr)
	}

	if err != nil {
		panic(err)
	}

	defer l.Close()
	fmt.Println(fmt.Sprintf("listening at: %v", l.Addr()))

	run := func(l net.Listener, s *http.Server) func() {
		c := make(chan struct{})

		go func() {
			defer close(c)

			if err := s.Serve(l); err != nil {
				log.Debug("failed to serve:", err)
			}
		}()

		return func() {
			ctx, cancel := context.WithTimeout(context.Background(), time.Duration(*timeout)*time.Second)
			defer cancel()

			// reason for timeout - shutdown issue: https://github.com/golang/go/issues/20239
			log.Debug("shutting down HTTP server..")
			s.Shutdown(ctx)
			log.Debug("HTTP server shutdown complete.")

			log.Debug("closing listener..")
			l.Close()
			log.Debug("listener closed.")

			<-c
		}
	}

	c := wh.HTTPHubConfig{}

	if *db != "" {
		s, err := wh.NewMongoSession(*db)
		if err != nil {
			panic(errors.Wrap(err, "failed to create mongo session"))
		}

		defer s.Clone()

		r, err := wh.NewMongoRequests(s, wh.MongoRequestsConfig{
			RequestsCollection: "requests",
		})
		if err != nil {
			panic(errors.Wrap(err, "failed to create init mongo requests"))
		}

		c.Requests = r

		a, err := wh.NewMongoAccounts(s, wh.MongoAccountsConfig{
			AccountsCollection: "accounts",
		})
		if err != nil {
			panic(errors.Wrap(err, "failed to init mongo accounts "))
		}

		c.Accounts = a

		auth, err := wh.NewMongoAuth(s, wh.MongoAuthConfig{
			AccountsCollection: "accounts",
		})
		if err != nil {
			panic(errors.Wrap(err, "failed to init mongo auth"))
		}

		c.BasicAuth = auth
	}

	mux := wh.NewHTTPHub(c)
	stop := run(l, &http.Server{
		Handler: mux,
	})

	<-interruptSignal()
	log.Debug("SIGINT signal received; shut down initiated")

	stop()

	log.Debug("shut down complete.")
}
