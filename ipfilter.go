package wh

import (
	"net"
	"strings"
	"wh/log"
)

type IPFilter interface {
	IsAllowed(ip string) bool
}

type singleIPFilter struct {
	ip net.IP
	n  *net.IPNet
}

type multiIPFilter struct {
	fs []IPFilter
}

func (f *multiIPFilter) IsAllowed(ip string) bool {
	if len(f.fs) == 0 {
		return true
	}

	for _, filter := range f.fs {
		if filter.IsAllowed(ip) {
			return true
		}
	}

	return false
}

func MakeIPFilter(s string) IPFilter {
	var fs []IPFilter

	items := strings.Split(s, ",")
	for _, item := range items {
		if strings.TrimSpace(item) != "" {
			sf := makeSingleIPFilter(item)

			if sf != nil {
				fs = append(fs, sf)
			}
		}
	}

	return &multiIPFilter{
		fs: fs,
	}
}

func makeSingleIPFilter(s string) *singleIPFilter {
	if !strings.Contains(s, "/") {
		log.Trace("simple match filter")

		return &singleIPFilter{
			ip: net.ParseIP(s),
		}
	}

	_, n, err := net.ParseCIDR(s)
	if err != nil {
		log.Debug("invalid CIDR:", s)
		return nil
	}

	log.Trace("CIDR filter")

	return &singleIPFilter{
		n: n,
	}
}

func (f *singleIPFilter) IsAllowed(ip string) bool {
	if f == nil {
		return true
	}

	if f.n == nil && f.ip == nil {
		return true
	}

	nip := net.ParseIP(ip)

	if f.n == nil {
		return f.ip.Equal(nip)
	}

	return f.n.Contains(nip)
}
