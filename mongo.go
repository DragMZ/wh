package wh

import (
	"crypto/tls"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"time"
	"wh/log"

	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type mongoRequests struct {
	s *mgo.Session
	c MongoRequestsConfig
}

type MongoRequestsConfig struct {
	RequestsCollection string
}

type MongoAccountsConfig struct {
	AccountsCollection string
}

type mongoAccounts struct {
	s *mgo.Session
	c MongoAccountsConfig
}

type mongoAccount struct {
	Name     string `bson:"name"`
	Password string `bson:"password"`
}

type MongoAuthConfig struct {
	AccountsCollection string
}

type mongoAuth struct {
	s *mgo.Session
	c MongoAuthConfig
}

type mongoAccountPassword struct {
	Password string `bson:"password"`
}

func (a *mongoAuth) Auth(user, password string) error {
	var p mongoAccountPassword

	err := a.s.DB("").C(a.c.AccountsCollection).Find(bson.M{"name": user}).One(&p)
	if err != nil {
		return errors.Wrap(err, "account not found")
	}

	err = bcrypt.CompareHashAndPassword([]byte(p.Password), []byte(password))
	if err != nil {
		return errors.Wrap(err, "password do not match")
	}

	return nil
}

func (a *mongoAccounts) Create(name, password string) error {
	// TODO: requires unique index on account name

	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return errors.Wrap(err, "failed to hash password")
	}

	return a.s.DB("").C(a.c.AccountsCollection).Insert(mongoAccount{
		Name:     name,
		Password: string(hash),
	})
}

func (a *mongoAccounts) Get(name string) (*Account, error) {
	var ac mongoAccount

	err := a.s.DB("").C(a.c.AccountsCollection).Find(bson.M{"name": name}).One(&ac)
	if err != nil {
		return nil, errors.Wrap(err, "failed to find account")
	}

	return &Account{
		ID: ac.Name,
	}, nil
}

func NewMongoSession(dburl string) (*mgo.Session, error) {
	var err error
	log.Trace("Mongo URL:", dburl)

	u, err := url.Parse(dburl)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse db url")
	}

	q := u.Query()

	ssl := q.Get("ssl")
	q.Del("ssl")

	var usessl bool
	if ssl != "" {
		usessl, err = strconv.ParseBool(ssl)
		if err != nil {
			return nil, errors.Wrap(err, "failed to parse ssl option")
		}
	}

	u.RawQuery = q.Encode()
	eurl := u.String()

	log.Trace("Mongo effective URL:", eurl, ", Use SSL:", usessl)

	di, err := mgo.ParseURL(eurl)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse URL")
	}

	log.Trace("Mongo DB:", di.Database)

	if usessl {
		di.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
			return tls.Dial("tcp", addr.String(), &tls.Config{})
		}
	}

	log.Trace("Mongo connection starting:", di)

	s, err := mgo.DialWithInfo(di)
	if err != nil {
		return nil, errors.Wrap(err, "failed to connect")
	}

	log.Trace("Mongo connection established.")

	return s, nil
}

func NewMongoRequests(s *mgo.Session, config MongoRequestsConfig) (RequestsDB, error) {
	r := &mongoRequests{
		s: s,
		c: config,
	}

	return r, nil
}

func NewMongoAccounts(s *mgo.Session, config MongoAccountsConfig) (AccountsDB, error) {
	a := &mongoAccounts{
		s: s,
		c: config,
	}

	return a, nil
}

func NewMongoAuth(s *mgo.Session, config MongoAuthConfig) (BasicAuthDB, error) {
	return &mongoAuth{
		s: s,
		c: config,
	}, nil
}

func (r *mongoRequests) Get(account string) ([]Request, error) {
	type requestHTTP struct {
		IP       string            `bson:"ip"`
		Method   string            `bson:"method"`
		URI      string            `bson:"uri"`
		Protocol string            `bson:"proto"`
		Headers  map[string]string `bson:"headers"`
	}

	type request struct {
		Hook      string      `bson:"hook"`
		At        time.Time   `bson:"at"`
		Listeners []string    `bson:"listeners"`
		Error     string      `bson:"error"`
		HTTP      requestHTTP `bson:"http"`
	}

	var reqs []request
	err := r.s.DB("").C(r.c.RequestsCollection).Find(bson.M{"account": account}).Sort("-at").Limit(100).All(&reqs)
	if err != nil {
		return []Request{}, errors.Wrap(err, "failed to query requests")
	}

	results := make([]Request, len(reqs))

	for i, req := range reqs {
		results[i] = Request{
			Hook:      req.Hook,
			At:        req.At.UTC(),
			Listeners: req.Listeners,
			Error:     req.Error,
			IP:        req.HTTP.IP,
			Method:    req.HTTP.Method,
			URI:       req.HTTP.URI,
			Protocol:  req.HTTP.Protocol,
			Headers:   req.HTTP.Headers,
		}
	}

	return results, nil
}

func (r *mongoRequests) Register(h *http.Request, t *TriggerRequest, res TriggerRequestResult) error {
	type requestHTTP struct {
		IP       string            `bson:"ip"`
		Method   string            `bson:"method"`
		URI      string            `bson:"uri"`
		Protocol string            `bson:"proto"`
		Headers  map[string]string `bson:"headers"`
	}

	type request struct {
		Account   string      `bson:"account"`
		Hook      string      `bson:"hook"`
		At        time.Time   `bson:"at"`
		Error     *string     `bson:"error"`
		Listeners []string    `bson:"listeners"`
		HTTP      requestHTTP `bson:"http"`
	}

	hdr := make(map[string]string)
	for k := range h.Header {
		hdr[k] = h.Header.Get(k)
	}

	mro := request{
		Account:   t.Account,
		Hook:      t.ID,
		At:        time.Now().UTC(),
		Listeners: res.Listeners,
		HTTP: requestHTTP{
			IP:       t.IP,
			Method:   h.Method,
			URI:      h.RequestURI,
			Protocol: h.Proto,
			Headers:  hdr,
		},
	}

	if res.Error != nil {
		s := res.Error.Error()
		mro.Error = &s
	}

	log.Trace("registering mongo request:", mro)

	err := r.s.DB("").C(r.c.RequestsCollection).Insert(&mro)

	if err != nil {
		return errors.Wrap(err, "failed to register request")
	}

	return nil
}

func (r *mongoRequests) Close() error {
	if r.s != nil {
		r.s.Close()
		r.s = nil
	}

	return nil
}
