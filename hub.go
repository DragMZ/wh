package wh

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"net/url"
	"sync"
	"wh/log"

	"github.com/pkg/errors"
)

func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// GenerateRandomString returns a URL-safe, base64 encoded
// securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func generateRandomString(s int) (string, error) {
	b, err := generateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}

type hookInfo struct {
	Hook
	id string
}

type hookReceiverFactories struct {
	c    chan struct{} // notification channel
	rcvm map[EventReceiverFactory]hookInfo
}

type registrationReceiverFactories struct {
	l *sync.Mutex
	m map[string]map[EventReceiverFactory]struct{}
}

type simpleHooksRegistry struct {
	l  *sync.RWMutex
	fm map[string]*hookReceiverFactories   // maps hook id to receiver factories
	rm map[string]*simpleHooksRegistration // maps registration id into a receiver factory
}

type simpleHooksRegistration struct {
	r      *simpleHooksRegistry
	rcv    EventReceiverFactory
	id     string
	cancel func()
}

func (r *simpleHooksRegistration) Unregister() error {
	r.r.l.Lock()
	defer r.r.l.Unlock()

	delete(r.r.rm, r.id)

	for _, v := range r.r.fm {
		delete(v.rcvm, r.rcv)
	}

	r.cancel()

	log.Trace("unregistered")

	return nil
}

func (r *simpleHooksRegistry) GetListeners() ([]Listener, error) {
	r.l.RLock()
	defer r.l.RUnlock()

	m := make(map[EventReceiverFactory]map[string]struct{})

	for hid, fs := range r.fm {
		for rcv := range fs.rcvm {
			mm, ok := m[rcv]
			if !ok {
				mm = make(map[string]struct{})
				m[rcv] = mm
			}
			mm[hid] = struct{}{}
		}
	}

	res := []Listener{}

	for k, v := range r.rm {
		hooks := []string{}

		for h := range m[v.rcv] {
			hooks = append(hooks, h)
		}

		res = append(res, Listener{
			ID:    k,
			Hooks: hooks,
		})
	}

	return res, nil
}

func (r *simpleHooksRegistry) Cancel(id string) error {
	reg, ok := r.rm[id]
	if !ok {
		return errors.New("registration not found")
	}

	return reg.Unregister()
}

func (r *simpleHooksRegistry) Register(hs []Hook, rcv EventReceiverFactory, cancel func()) (HooksRegistration, error) {
	log.Debug("registering hooks:", hs)

	r.l.Lock()
	defer r.l.Unlock()

	var id string
	for {
		var err error

		id, err = generateRandomString(32)
		if err != nil {
			panic(errors.Wrap(err, "failed to generate registration id"))
		}

		_, ok := r.rm[id]
		if !ok {
			break
		}
	}

	reg := &simpleHooksRegistration{
		r:   r,
		rcv: rcv,
		id:  id,
		cancel: func() {
			log.Trace("cancelling")
			cancel()
			log.Trace("cancelled")
		},
	}

	r.rm[id] = reg

	for _, h := range hs {
		rh := r.unsafeGet(h.ID)
		rh.rcvm[rcv] = hookInfo{
			Hook: h,
			id:   id,
		}

		close(rh.c) // notify waiters
		rh.c = make(chan struct{})
	}

	return reg, nil
}

func (r *simpleHooksRegistry) unsafeGet(id string) *hookReceiverFactories {
	h, ok := r.fm[id]
	if !ok {
		h = &hookReceiverFactories{
			rcvm: make(map[EventReceiverFactory]hookInfo),
			c:    make(chan struct{}),
		}

		r.fm[id] = h
	}

	return h
}

func (r *simpleHooksRegistry) Trigger(ctx context.Context, t *Trigger) (map[string][]string, error) {
	type item struct {
		r EventReceiver
		h hookInfo
	}

	var rcvs []item
	var nch chan struct{}

	for {
		func() {
			r.l.Lock()
			defer r.l.Unlock()

			hr := r.unsafeGet(t.ID)
			nch = hr.c

			for f, h := range hr.rcvm {
				if !h.match(t) {
					continue
				}

				rcv, err := f.Create()
				if err != nil {
					log.Debug("receiver factory failed:", err)
					continue
				}

				rcvs = append(rcvs, item{
					r: rcv,
					h: h,
				})
			}
		}()

		lc := len(rcvs)
		log.Debug("trigger receivers:", lc)

		if lc > 0 {
			break
		}

		if !t.Wait {
			return nil, errors.New("no trigger receivers")
		}

		log.Debug("waiting for trigger receivers")

		select {
		case <-ctx.Done():
			log.Debug("failed to wait for trigger receivers")
			return nil, errors.Wrap(ctx.Err(), "no trigger receivers")
		case <-nch:
			log.Debug("got wait channel notification")
		}
	}

	idsm := make(map[string][]string)

	for _, rcv := range rcvs {
		e := Event{
			ID:          t.ID,
			ContentType: t.Type,
		}

		if rcv.h.Body {
			r, err := t.Data.Create()
			if err != nil {
				log.Debug("failed to create trigger data reader:", err)
				continue
			}

			e.Data = r
		}

		if rcv.h.Args {
			e.Args = t.Args
		}

		if err := rcv.r.Send(ctx, &e); err != nil {
			log.Debug("trigger error:", err)
			continue
		}

		idsm[t.ID] = append(idsm[t.ID], rcv.h.id)
	}

	return idsm, nil
}

type simpleAccount struct {
	r *simpleHooksRegistry
}

func (a *simpleAccount) Hooks() (HooksRegistry, error) {
	return a.r, nil
}

func (a *simpleAccount) Triggers() (TriggersRegistry, error) {
	return a.r, nil
}

type simpleAccounts struct {
	l *sync.Mutex
	m map[string]*simpleAccount
}

// NewSimpleAccounts returns simple implementation of account hooks and triggers
func NewSimpleAccounts() (AccountHooks, AccountTriggers) {
	a := &simpleAccounts{
		l: &sync.Mutex{},
		m: make(map[string]*simpleAccount),
	}

	return &simpleAccountHooks{
			a: a,
		}, &simpleAccountTriggers{
			a: a,
		}
}

type simpleAccountHooks struct {
	a *simpleAccounts
}

func (h *simpleAccountHooks) Get(account string) (HooksRegistry, error) {
	a, err := h.a.Get(account)
	if err != nil {
		return nil, errors.Wrap(err, "failed to found account")
	}

	return a.r, nil
}

type simpleAccountTriggers struct {
	a *simpleAccounts
}

func (t *simpleAccountTriggers) Get(account string) (TriggersRegistry, error) {
	a, err := t.a.Get(account)
	if err != nil {
		return nil, errors.Wrap(err, "failed to found account")
	}

	return a.r, nil
}

func (a *simpleAccounts) Get(id string) (*simpleAccount, error) {
	a.l.Lock()
	defer a.l.Unlock()

	acc, ok := a.m[id]
	if !ok {
		acc = &simpleAccount{
			r: &simpleHooksRegistry{
				l:  &sync.RWMutex{},
				fm: make(map[string]*hookReceiverFactories),
				rm: make(map[string]*simpleHooksRegistration),
			},
		}
		a.m[id] = acc
	}

	return acc, nil
}

type AccountServer struct {
	Accounts AccountsDB
	Requests RequestsDB
	Hooks    AccountHooks
}

// TriggerServer is a core implementation of trigger server used to trigger events
type TriggerServer struct {
	Triggers AccountTriggers
}

// EventsServer is a core implementation of a events server used to receive events
type EventsServer struct {
	Hooks AccountHooks
}

type Filters map[string]string

func (f Filters) Match(v url.Values) bool {
	for fk, fv := range f {
		av := v.Get(fk)
		if av != fv {
			log.Debug("failed filter:", fk, ", expected:", fv, ", got:", av)
			return false
		}
	}

	return true
}

type HookConfig struct {
	ID       string            `json:"id"`
	Args     bool              `json:"args"`
	Filters  map[string]string `json:"filters"`
	Body     bool              `json:"body"`
	IPFilter string            `json:"ip"`
}

type Hook struct {
	ID string // hook id

	Args bool // should pass args from trigger

	Body bool // should pass body

	IPFilter    IPFilter
	QueryFilter Filters // required args and their values
}

func (h Hook) match(t *Trigger) bool {
	if h.IPFilter != nil {
		if !h.IPFilter.IsAllowed(t.IP) {
			log.Trace("hook IP mismatch")
			return false
		}
	}

	if !h.QueryFilter.Match(t.Args) {
		log.Trace("hook filter mismatch")
		return false
	}

	return true
}

// ListenRequest describes a listen request
type ListenRequest struct {
	Account string
	Hooks   []Hook
}

// Trigger describes a webhook trigger
type Trigger struct {
	ID   string
	IP   string // source IP
	Type string
	Args url.Values
	Data ReaderFactory

	Wait bool // should wait for any receiver?
}

// TriggerRequest describes a trigger request
type TriggerRequest struct {
	Trigger
	Account string
}

// Trigger triggers a webhook event
func (s *TriggerServer) Trigger(ctx context.Context, req *TriggerRequest) (map[string][]string, error) {
	t, err := s.Triggers.Get(req.Account)
	if err != nil {
		return nil, errors.Wrapf(err, "triggers not found for account: %s", req.Account)
	}

	lids, err := t.Trigger(ctx, &req.Trigger)
	if err != nil {
		return nil, errors.Wrap(err, "failed to trigger")
	}

	return lids, nil
}

// Listen starts listening for incoming events
func (s *EventsServer) Listen(req *ListenRequest, out EventReceiverFactory, cancel func()) (func() error, error) {
	hooks, err := s.Hooks.Get(req.Account)
	if err != nil {
		return nil, errors.Wrapf(err, "hooks not found for account: %s", req.Account)
	}

	r, err := hooks.Register(req.Hooks, out, cancel)
	if err != nil {
		return nil, errors.Wrap(err, "failed to register event receiver for hooks")
	}

	return r.Unregister, nil
}

func (s *AccountServer) Create(name, password string) error {
	_, err := s.Accounts.Get(name)
	if err == nil {
		return errors.New("account already exists")
	}

	err = s.Accounts.Create(name, password)
	if err != nil {
		return errors.Wrap(err, "failed to create account")
	}

	return nil
}

func (s *AccountServer) Get(name string) (*Account, error) {
	a, err := s.Accounts.Get(name)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get account")
	}

	return a, nil
}

type AccountDetails struct {
	Name      string     `json:"name"`
	Listeners []Listener `json:"listeners"`
	Requests  []Request  `json:"requests"`
}

func (s *AccountServer) GetAccountDetails(name string) (AccountDetails, error) {
	hr, err := s.Hooks.Get(name)
	if err != nil {
		return AccountDetails{}, errors.Wrap(err, "failed to get hooks registry")
	}

	hooks, err := hr.GetListeners()
	if err != nil {
		return AccountDetails{}, errors.Wrap(err, "failed to get registered hooks")
	}

	reqs := []Request{}

	if s.Requests != nil {
		reqs, err = s.Requests.Get(name)
		if err != nil {
			return AccountDetails{}, errors.Wrap(err, "failed to get last account requests")
		}
	}

	return AccountDetails{
		Name:      name,
		Listeners: hooks,
		Requests:  reqs,
	}, nil
}
