package wh

import "testing"

func TestIPFilter(t *testing.T) {
	tests := []struct {
		a string
		b string
		r bool
	}{
		{"", "", true},
		{"", "asd", true},
		{"", "127.0.0.1", true},
		{"127.0.0.1", "", false},
		{"127.0.0.1", "asd", false},
		{"127.0.0.1", "127.0.0.1", true},
		{"127.0.0.1/32", "", false},
		{"127.0.0.1/32", "asd", false},
		{"127.0.0.1/32", "127.0.0.1", true},
	}

	for i, test := range tests {
		if MakeIPFilter(test.a).IsAllowed(test.b) != test.r {
			t.Errorf("match %d fail: %s vs %s", i, test.a, test.b)
		}
	}
}
