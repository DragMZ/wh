package wh

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func trigger(h http.Handler) {
	h.ServeHTTP(httptest.NewRecorder(), httptest.NewRequest("POST", "/trigger/a/testhook", strings.NewReader("test data")))
}

func listenPOST(h http.Handler, hooks []HookConfig) *httptest.ResponseRecorder {
	data, _ := json.Marshal(hooks)
	req := httptest.NewRequest("POST", "/event/a", bytes.NewReader(data))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	w := httptest.NewRecorder()

	h.ServeHTTP(w, req)

	return w
}

func listenGET(h http.Handler) *httptest.ResponseRecorder {
	req := httptest.NewRequest("GET", "/event/a/testhook", nil)
	w := httptest.NewRecorder()

	h.ServeHTTP(w, req)

	return w
}

func newHTTPHub() http.Handler {
	return NewHTTPHub(HTTPHubConfig{})
}

func BenchmarkHTTPHub(b *testing.B) {
	h := newHTTPHub()

	for n := 0; n < b.N; n++ {
		go trigger(h)
		listenGET(h)
	}
}

func TestHTTPHubPOST(t *testing.T) {
	h := newHTTPHub()

	go trigger(h)
	w := listenPOST(h, []HookConfig{
		{
			ID: "testhook",
		},
	})

	if w.Code != 200 {
		t.Errorf("unexpected code: %d", w.Code)
	}
}

func TestHTTPHub(t *testing.T) {
	h := newHTTPHub()

	go trigger(h)
	w := listenGET(h)

	if w.Code != 200 {
		t.Errorf("unexpected code: %d", w.Code)
	}

	id := w.Header().Get("X-Hook-ID")
	if id != "testhook" {
		t.Errorf("unexpected hook: %s", id)
	}

	b, _ := ioutil.ReadAll(w.Body)
	s := string(b)

	if s != "test data" {
		t.Errorf("unexpected body: %s", s)
	}
}
