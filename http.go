package wh

import (
	"net/http"
	"strings"
	"sync"
	"wh/log"

	"github.com/pkg/errors"
)

func httpError(w http.ResponseWriter, err string, code int) {
	http.Error(w, err, code)
	log.Debug("HTTP error:", code, err)
}

func readPathParts(path string) []string {
	raw := strings.Split(path, "/")

	var parts []string

	for _, part := range raw {
		if part != "" {
			parts = append(parts, part)
		}
	}

	return parts
}

type httpMuxHandler struct {
	prefix  string
	handler http.Handler
}

type httpMux struct {
	handlers []*httpMuxHandler
}

func (m *httpMux) Handle(prefix string, handler http.Handler) {
	m.handlers = append(m.handlers, &httpMuxHandler{
		prefix:  prefix,
		handler: http.StripPrefix(prefix, handler),
	})
}

func (m *httpMux) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	log.Trace(req)

	for _, h := range m.handlers {
		if strings.HasPrefix(req.URL.Path, h.prefix) {
			h.handler.ServeHTTP(w, req)
			return
		}
	}

	http.Error(w, "not found", http.StatusNotFound)
}

type HTTPHubConfig struct {
	Requests  RequestsDB
	Accounts  AccountsDB
	BasicAuth BasicAuthDB
}

func NewHTTPHub(c HTTPHubConfig) http.Handler {
	ah, at := NewSimpleAccounts()

	mux := &httpMux{}

	mux.Handle("/event/", &HTTPEventsHandler{
		Server: &EventsServer{
			Hooks: ah,
		},
	})

	mux.Handle("/trigger/", &HTTPTriggerHandler{
		Requests: c.Requests,
		Server: &TriggerServer{
			Triggers: at,
		},
	})

	mux.Handle("/account/", &HTTPAccountHandler{
		BasicAuth: c.BasicAuth,
		Server: &AccountServer{
			Accounts: c.Accounts,
			Hooks:    ah,
			Requests: c.Requests,
		},
	})

	return mux
}

type singleEventReceiverFactory struct {
	l *sync.Mutex
	r EventReceiver
}

func (f *singleEventReceiverFactory) Create() (EventReceiver, error) {
	f.l.Lock()
	defer f.l.Unlock()

	if f.r == nil {
		return nil, errors.New("cannot receive more events")
	}

	r := f.r
	f.r = nil // make the receiver nil so no more receivers can be produced by this factory (because it's a single receiver factory)

	return r, nil
}
